import flask
from flask_cors import CORS, cross_origin
from flask import request, jsonify
from SPARQLWrapper import SPARQLWrapper, JSON
from utils import create_object, create_hotels_obj

app = flask.Flask(__name__)
cors = CORS(app)
app.config["DEBUG"] = True
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/', methods=['GET'])
@cross_origin()
def home():
    return "<h1>Distant Reading Archive</h1><p>This site is a prototype API for distant reading of science fiction novels.</p>"


@app.route('/hotels', methods=['GET'])
@cross_origin()
def get_hotel():
    if "name" in request.args:
        name = str(request.args["name"])
        sparql = SPARQLWrapper("http://localhost:3030/ds/sparql")
        sparql.setQuery("""
        PREFIX ex:  <http://ex.org/a#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX dbr: <http://dbpedia.org/resource/>
            SELECT DISTINCT *
            WHERE {
            ?subject ex:Name "%s";
                ex:Address ?address ;
                ex:Category dbr:Hotel ;
                ex:City ?city ;
                ex:country ?country ;
                ex:Latitude ?latitude ;
                ex:Longitude ?longitude ;
                ex:PostalCode ?postal_code ;
                ex:Province ?province ;
                ex:HasReviews ?review .
  
            ?review ex:ReviewsRating ?rating ;
                    ex:ReviewsText ?text ;
                    ex:ReviewsTitle ?title ;
                    ex:ReviewsUsername ?user .
            }
            
        """ % name)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        #print(results["results"]["bindings"][0]["subject"])
        #create_object(results, name)
        return jsonify(create_object(results, name))
    else:
        return "Error: No parameter specified."
    
@app.route('/province', methods=['GET'])
@cross_origin()
def get_hotels_from_province():
    if "province" in request.args:
        province = str(request.args["province"])
        sparql = SPARQLWrapper("http://localhost:3030/ds/sparql")
        sparql.setQuery("""
        PREFIX ex:  <http://ex.org/a#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX dbr: <http://dbpedia.org/resource/>
            SELECT DISTINCT *
            WHERE {
                ?subject ex:Province "%s";
                        ex:Name ?name .
            }
        """ % province)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        return jsonify(create_hotels_obj(results))
    else:
        return "Error: No parameter specified."


app.run()
