import json


# Create object using data from SPARQLWrapper post request
def create_object(obj, name):
    print((obj["results"]["bindings"]))
    binding_length = len(obj["results"]["bindings"])
    response = {}
    if binding_length == 0:
        response = {
            "status": 404,
        }
        return response
    else :
        obj_data = obj["results"]["bindings"][0]
        response = {
            "status": 200,
            "data": {
                "address" : {
                    "type": "literal",
                    "value" : obj_data["address"]["value"]
                },
                "category" : {
                    "type": "uri",
                    "value": "http://dbpedia.org/resource/Hotel"
                },
                "city": {
                    "type": "literal",
                    "value": obj_data["city"]["value"]
                },
                "country": {
                    "type": "uri",
                    "value": obj_data["country"]["value"]
                },
                "latitude": {
                    "type": "literal",
                    "value": obj_data["latitude"]["value"]
                },
                "longitude": {
                    "type": "literal",
                    "value": obj_data["longitude"]["value"]
                },
                "postal_code": {
                    "type": "literal",
                    "value": obj_data["postal_code"]["value"]
                },
                "province": {
                    "type": "literal",
                    "value": obj_data["province"]["value"]
                },
                "name": {
                    "type": "literal",
                    "value": name
                }, 
                "reviews": []
            }
        }

        for review in obj["results"]["bindings"]:
            print(review)
            response["data"]["reviews"].append({
                "text": review["text"]["value"],
                "rating": int(review["rating"]["value"]),
                "username" : review["user"]["value"],
                "title": review["title"]["value"]
            })

        return response
    
def create_hotels_obj(obj):
    binding_length = len(obj["results"]["bindings"])
    response = {}
    if binding_length == 0:
        response = {
            "status": 404,
        }
        return response
    else:
        response = {
            "data": [],
            "status": 200
        }
        for hotel in obj["results"]["bindings"]:
            response["data"].append({
                "name": hotel["name"]["value"]
            })
        return response