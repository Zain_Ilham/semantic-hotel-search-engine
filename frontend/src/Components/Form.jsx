import React from 'react';
import './CSS/Form.css'

const Form = (props) => {
    return (
        <div>
            <form onSubmit={props.onSubmit}>
                <input type="text" name="hotel" value={props.name} onChange={props.onChange}/>
            </form>
        </div>
    )
}

export default Form;
