import React from 'react';
import './CSS/Review.css';

const Review = (props) => {
    return (
        <div className="reviews">
             <h3 className="title-reviews">Reviews :</h3>
             { props.reviews.map(review => {
                 return (
                     <div className="review">
                         <h4>{review.title}</h4>
                         <p>by : {review.username}</p>
                         <p>Rating : {review.rating}</p>
                         <p>"{review.text}"</p>
                     </div>
                 )
             })};
        </div>
    )
}

export default Review;
