import React from 'react';
import './CSS/Button.css';


const Button = (props) => {
    
    return (
        <div className="buttonContainer">
            <button className="button" onClick={props.onClick}>
                Search
            </button>
        </div>
    )
}

export default Button;