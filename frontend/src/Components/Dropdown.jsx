import React, {useState} from "react";
import Select from 'react-select';
import './CSS/Dropdown.css';

const Dropdown2 = (props) => {
    return (
        <div className="dropdownContainer">
            <Select className="dropdown"
                value={props.selectedOpt}
                onChange={props.onChange}
                options={props.options}
            />
        </div>
    );
};

export default Dropdown2;
