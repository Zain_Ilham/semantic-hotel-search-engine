import React from "react";
import './CSS/MultipleResultPage.css';

const MultipleResultPage = props => {
  const data = props.data;

  if (props.error === true) {
    return <div className="status">Oops... looks like there's an error</div>;
    // No server error
  } else {
    // Check fetch status
    if (props.status === "idle") {
      return <div></div>;
    } else if (props.status === "fetch") {
      return <div className="status">Searching Hotel...</div>;
    } else {
      // Check if hotel is found or not
      if (props.data.status === 404) {
        return <div className="status">No Hotel Found</div>;
      } else if (props.data.status === 200) {
        const data = props.data.data;
        return data.map(elem => {
          return (
               <li><p>{elem.name}</p></li>
            );
        });
      }
    }
  }
};

export default MultipleResultPage;
