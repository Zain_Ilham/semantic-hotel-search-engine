import HomePage from './HomePage.jsx';
import ResultPage from './ResultPage.jsx';
import MultipleResultPage from './MultipleResultPage.jsx';

export { HomePage, ResultPage, MultipleResultPage }