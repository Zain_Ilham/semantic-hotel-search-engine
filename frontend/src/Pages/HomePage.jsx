import React, { useState } from "react";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { Button, Form, Dropdown2 } from "../Components";
import { ResultPage, MultipleResultPage } from "./index";
import "./CSS/HomePage.css";

const HomePage = () => {
  const options = [
    { value: 'AK', label: 'Alaska' },
    { value: 'Atl', label: 'Atlanta' },
    { value: 'Bunker Hill', label: 'Bunker Hill' },
    { value: 'Cumberland', label: 'Cumberland' },
    { value: 'Canoe Camp', label: 'Canoe Camp' },
    { value: 'Collider Twp', label: 'Collider Twp' },
    { value: 'Copper Queen', label: 'Copper Queen' },
    { value: 'DE', label: 'Delaware' },
    { value: 'Englewood', label: 'Englewood' },
    { value: 'Eulalia', label: 'Eulalia' },
    { value: 'Ela', label: 'Ela' },
    { value: 'FL', label: 'Florida' },
    { value: 'Flournoy', label: 'Flournoy' },
    { value: 'HI', label: 'Hawaii' },
    { value: 'Htfd', label: 'Htfd' },
    { value: 'ID', label: 'Idaho' },
    { value: 'Jackson Hole', label: 'Jackson Hole' },
    { value: 'Los Ranchos De Albuquerque', label: 'Los Ranchos De Albuquerque' },
    { value: 'Lk Lotawana', label: 'Lk Lotawana' },
    { value: 'MT', label: 'Montana' },
    { value: 'NE', label: 'Nebraska' },
    { value: 'N Richland Hills', label: 'N Richland Hills' },
    { value: 'Pandora', label: 'Pandora' },
    { value: 'Pontotoc', label: 'Pontotoc' },
    { value: 'Port Everglades', label: 'Port Everglades' },
    { value: 'South Mountain', label: 'South Mountain' },
    { value: 'TX', label: 'Texas' },
    { value: 'Toolesboro', label: 'Toolesboro' },
    { value: 'WA', label: 'Washington' },
    { value: 'Woodbury', label: 'Woodbury' },
  ];

  const [status, setStatus] = useState("idle");
  const [status2, setStatus2] = useState("idle");
  const [state, setState] = useState({});
  const [name, setName] = useState("");
  const [error, setError] = useState(false);
  const [error2, setError2] = useState(false);
  const [province, setProvince] = useState(null);
  const [hotels, setHotels] = useState(null);
  const onNameChange = e => {
    e.preventDefault();
    setName(e.target.value);
  };

  const handleSubmit = e => {
    e.preventDefault();
    setStatus("fetch");
    fetch(`http://localhost:5000/hotels?name=${name}`)
      .then(response => response.json())
      .then(data => {
        setState(data);
        setStatus("done")
        setError(false)
      })
      .catch(e => {
        console.log(e);
        setError(true)
      });
  };

  const handleSubmit2 = (selectedOption) => {
    setProvince(selectedOption)
    setStatus2("fetch");
    fetch(`http://localhost:5000/province?province=${selectedOption.value}`)
      .then(response => response.json())
      .then(data => {
        setHotels(data);
        console.log(hotels)
        setStatus2("done")
        setError2(false)
      })
      .catch(e => {
        console.log(e);
        setError2(true)
      });
  };

  return (
    <React.Fragment>
      <Tabs>
        <TabList>
          <Tab> Search Hotel by Name</Tab>
          <Tab>Search Hotel by Province</Tab>
        </TabList>

        <TabPanel>
            <div className="wrapper">
              <h1>United States of Hotels</h1>
              <Form value={name} onChange={onNameChange} onSubmit={handleSubmit} />
              <Button onClick={handleSubmit} />
            </div>
          <ResultPage data={state} status={status} error={error} />
        </TabPanel>
          
        <TabPanel>
          <div className="wrapper">
            <h1>United States of Hotels</h1>
            <Dropdown2 selectedOpt={province} onChange={handleSubmit2} options={options}/>
          </div>
          <div className="multipleResultContainer">
            <ul className="listMultiple">
              <MultipleResultPage data={hotels} status={status2} error={error2} />
            </ul>
          </div>
        </TabPanel> 
      </Tabs>

    </React.Fragment>


  );
};

export default HomePage;
