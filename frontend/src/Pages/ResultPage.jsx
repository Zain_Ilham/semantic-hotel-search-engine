import React from "react";
import "./CSS/ResultPage.css";
import { Review } from '../Components'

const ResultPage = props => {
  const data = props.data.data;

  if (props.error === true) {
    return <div className="status">Oops... looks like there's an error</div>;
    // No server error
  } else {
    // Check fetch status
    if (props.status === "idle") {
      return <div></div>;
    } else if (props.status === "fetch") {
      return <div className="status" >Searching Hotel...</div>;
    } else {
      // Check if hotel is found or not
      if (props.data.status === 404) {
        return <div className="status" >No Hotel Found</div>;
      } else if (props.data.status === 200) {
        return (
          <div className="card">
            <h2 className="name"> {data.name.value} </h2>
            <hr/>
            {/* Loop hotel data  */}
            {Object.keys(data).map(key => {
              // Check for special case (name or reviews)
              if (key === "name") {
                
              } else if (key === "reviews") {
                return ( 
                  <Review reviews={data[key]} />
                )
              } else {
                // check if data type is a link or not
                if (data[key].type === "literal") {
                  return (
                    <p className="detail" key={key}>
                      {key.charAt(0).toUpperCase()}{key.slice(1)}: {data[key].value}
                    </p>
                  );
                } else if (data[key].type === "uri") {
                    const splitUrl = data[key].value.split("/");
                    const newValue = splitUrl[splitUrl.length - 1]
                  return (
                    <p key={key} className="detail">
                      {key.charAt(0).toUpperCase()}{key.slice(1)}:{" "}
                      <span>
                        <a href={data[key].value}>{newValue}</a>
                      </span>
                    </p>
                  );
                }
              }
            })}
          </div>
        );
      }
    }
  }
};

export default ResultPage;
